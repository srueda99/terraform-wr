## Usage
Create your variables file, for example **dev.tfvars**,
the content of this file should be like this:
````
template_id = "id_of_your_launch_template"
````
Then create a file to store the AWS credentials, it can be called like **creds.auto.tfvars**, and the content would be like this:
````
AWS_ACCESS_KEY = "your_access_key"
AWS_SECRET_KEY = "your_secret_key"
AWS_TOKEN      = "your_session_token"
AWS_REGION     = "aws_region_to_use"
````
After this files are created, just run this command to start the terraform environment.
````
terraform init
````
To apply the terraform configuration just run this command.
````
terraform apply -var-file=dev.tfvars
````

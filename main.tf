# Setting up Artifactory Server
provider "aws" {
  region     = var.AWS_REGION
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
  token      = var.AWS_TOKEN
}

# Creating the EC2 Instance from the template
resource "aws_instance" "artifactory" {
  launch_template {
    id = var.template_id
  }
}

# Elastic IP address for the server
resource "aws_eip" "jfrog_ip" {
  instance = aws_instance.artifactory.id
  vpc      = true
  tags = {
    Name = "artifactory-terraform"
  }
}

# Shows the public IP of the instace in the output
output "instance_ip" {
  value = aws_eip.jfrog_ip.address
}